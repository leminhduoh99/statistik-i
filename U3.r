library("pacman")
pacman:: p_load("janitor", "tidyverse", "dplyr", "ggplot2")
table <- read.table("~/R_coding/jump_length_raw.txt")
table <- table %>% row_to_names(row_number = 1)
table

delim <- read.delim("~/R_coding/jump_length_raw.txt")
delim

vityl1 <- table %>% tabyl(vitality)
vityl1

vitly1 <- table %>% group_by(vitality) %>% tally()
vitly1

vitle1 <- table(table$vitality)
vitle1

table[!duplicated(table$id),]
table

table$type <- replace(table$vitality, table$vitality<0 && table$vitality>9, NA)
table


vityl2 <- table %>% tabyl(vitality)
vityl2

vitly2 <- table %>% group_by(vitality) %>% tally()
vitly2

vitle2 <- table(table$vitality)
vitle2

genyl1 <- table %>% tabyl(gender)
genyl1

genly1 <- table %>% group_by(gender) %>% tally()
genly1

genle1 <- table(table$gender)
genle1

table$gender <- replace(table$gender, table$gender == "fale", "female")
table

histm <- ggplot(data = table, aes(x = weight, color = gender)) + geom_histogram(stat="count", bins=15)
histm


barc <- ggplot (data = table, aes(x = gender, y = weight)) + geom_bar(stat="identity", position = position_dodge2())
barc

